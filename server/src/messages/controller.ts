import Hapi from '@hapi/hapi';
import Joi from 'joi';

import { getAll, create } from './service';
import { JoiChannelName } from "../channels/validation";
import { JoiMessageContent, JoiUserName } from "./validation";

export default {
    name: 'messages',
    register: async function (server: Hapi.Server) {
      server.route([
        {
          method: 'GET',
          path: '/messages/{channel}/getAll',
          handler: async (req, h) => {
            return await getAll(req.params.channel);
          },
          options: {
            validate: {
              params: Joi.object({
                channel: JoiChannelName
              }),
            },
          },
        },
        {
          method: 'POST',
          path: '/messages/{channel}/post',
          handler: async (req, h) => {
            const payload = req.payload as any; //TODO
            console.log(payload);

            try {
              const res = await create(payload.content, payload.username, req.params.channel);
              console.log(res);
              return 200; //TODO proper return
            } catch(e) {

              console.log(e);
              throw new Error();
            }
          },
          options: {
            validate: {
              params: Joi.object({
                channel: JoiChannelName
              }),
              payload: Joi.object({
                username: JoiUserName,
                content: JoiMessageContent,
              })
            }
          }
        }
      ])
    }
};
