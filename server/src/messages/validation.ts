import Joi from 'joi';

export const JoiUserName = Joi.string().min(1).max(30).required();
export const JoiMessageContent = Joi.string().min(1).max(10000).required();
