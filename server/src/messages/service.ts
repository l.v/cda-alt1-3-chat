import { PrismaClient } from '@prisma/client'
import { get as getChatServer } from "../chatserver";

const prisma = new PrismaClient()

export const create = async (content: string, username: string, channelName: string) => {
    const res = await prisma.message.create({
        data: {
            content,
            username,
            channel: {
                connectOrCreate: {
                    where: {name: channelName},
                    create: {name: channelName},
                }
            }
        },
    })

    getChatServer()?.to(channelName).emit('message', res)
}

export const getAll = async (channelName: string) => {
    const msgs = await prisma.channel.findUnique({where: {name: channelName}}).messages();

    return msgs;
}
