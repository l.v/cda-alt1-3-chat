import { Server, Socket } from "socket.io";
import * as http from 'http';
import { create as createMessage } from './messages/service';

//TODO
let io : Server | null = null;

export const setup = (httpServer: http.Server) => {
    io = new Server(httpServer, {
        cors: {
            origin:["http://localhost:8080"],
            credentials: true
        }
    });
    
    io.on("connection", onConnection);
}

export const get = () => {
    return io;
}

const onConnection = (socket: Socket) => {
    console.log("user joined");

    let channelName = "";
    let channelSet = false;
    let username = "Anonymous";

    socket.on('join', (joinData) => {
        //avoid setting eventlisteners multiple times
        if (channelSet) return;
        
        channelSet = true;
        channelName = joinData.channelName;
        username = joinData.username;

        io?.to(channelName).emit('join', {username})
        socket.join(channelName);


        socket.on('disconnect', () => {
            io?.to(channelName).emit('leave', {username})
            socket.leave(channelName);
        })
        socket.on('leave', () => {
            io?.to(channelName).emit('leave', {username})
            socket.leave(channelName);
        })

        socket.on('alias', (newName) => {
            io?.to(channelName).emit('alias', {old: username, new: newName})
            username = newName;
        });

        socket.on('typing', () => {
            io?.to(channelName).emit('typing', {username})
        });

        socket.on('typingStop', () => {
            io?.to(channelName).emit('typingStop', {username})
        });

        socket.on('message', ({content}) => {
            createMessage(content, username, channelName);
        })
    })
}
