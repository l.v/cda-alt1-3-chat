import { server } from '@hapi/hapi'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

import { setup, getServer } from './server';
import { setup as setupChat } from './chatserver'

const hapiServer = getServer();
setupChat(hapiServer.listener);


process.on('unhandledRejection', err => {
  console.log(err)
  process.exit(1)
});

(async () => {
  const server = await setup();
  console.log(`Server running on ${server.info.uri}`)
})();
