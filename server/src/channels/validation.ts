import Joi from 'joi';

export const JoiChannelName = Joi.string().min(1).max(60).required();
