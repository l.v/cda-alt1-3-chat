import Hapi from '@hapi/hapi';
import Joi from 'joi';

import { JoiChannelName } from "./validation";
import { getAll, create } from './service';

export default {
    name: 'channels',
    register: async function (server: Hapi.Server) {
      server.route([
        {
          method: 'GET',
          path: '/channels/getAll',
          handler: async (req, h) => {
            return await getAll();
          }
        },

        {
          method: 'POST',
          path: '/channels/create',
          handler: async (req, h) => {
            const payload = req.payload as any; //TODO

            try {
              await create(payload.name);
              return 200; //TODO proper return
            } catch(e) {
              if (e.code === 'P2002') {
                //TODO proper return
                return 400; //already exists
              }

              console.log(e);
              throw new Error();
            }
          },
          options: {
            validate: {
              payload: Joi.object({
                name: JoiChannelName
              })
            }
          }
        },
      ])
    }
};
