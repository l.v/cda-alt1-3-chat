import Hapi from '@hapi/hapi'
import ChannelsController from './channels/controller'
import MessagesController from "./messages/controller";

const server: Hapi.Server = Hapi.server({
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'localhost',
    routes: {
        cors: true
    }
})



export async function setup(): Promise<Hapi.Server> {
    await server.register([ChannelsController, MessagesController]);
    await server.start();
    return server
}

export const getServer = () => {
    return server;
}
