# back
```
cd server
npm install
npx prisma generate
npx prisma migrate dev --name init --preview-feature
DEBUG=socket.io:client npm run dev
```

# front
```
cd client
yarn install
yarn serve
```

# test de requêtes http avec httpie
```
apt install httpie
http -j POST localhost:3000/channels/create name=somechannel
http localhost:3000/messages/somechannel/getAll
http -j POST localhost:3000/messages/somechannel/post username=myname content=hi
```